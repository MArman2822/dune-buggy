// fot home slider section
var indexValue = 1;
showImg(indexValue);

// Autoplay function
function autoPlay() {
  side_slide(1); // Advance to the next slide
}

// Set interval for autoplay (adjust the duration as needed)
setInterval(autoPlay, 4000); // Change slide every 3 seconds

function btm_slide(e) {
  showImg((indexValue = e));
}
function side_slide(e) {
  showImg((indexValue += e));
}
function showImg(e) {
  var i;
  const img = document.querySelectorAll(".heroSlider img");
  const slider = document.querySelectorAll(".btm-slides span");
  if (e > img.length) {
    indexValue = 1;
  }
  if (e < 1) {
    indexValue = img.length;
  }
  for (i = 0; i < img.length; i++) {
    img[i].style.display = "none";
  }
  for (i = 0; i < slider.length; i++) {
    slider[i].style.background = "rgba(255,255,255,0.1)";
  }
  img[indexValue - 1].style.display = "block";
  slider[indexValue - 1].style.background = "white";
}

// for coursel
$(".owl-1").owlCarousel({
  autoplay: true,
  autoplayTimeout: 2000,
  autoplayHoverPause: true,
  animateOut: "slideOutDown",
  animateIn: "flipInX",
  loop: true,
  margin: 10,
  nav: true,
  responsive: {
    0: {
      items: 1,
    },
    600: {
      items: 3,
    },
    1000: {
      items: 4,
    },
  },
});

$(".owl-2").owlCarousel({
  autoplay: true,
  autoplayTimeout: 3000,
  autoplayHoverPause: true,
  animateOut: "slideOutDown",
  animateIn: "flipInX",
  loop: true,
  margin: 10,
  nav: false,
  responsive: {
    0: {
      items: 1,
    },
    600: {
      items: 3,
    },
    1000: {
      items: 5,
    },
  },
});

$(".owl-3").owlCarousel({
  autoplay: true,
  autoplayTimeout: 3000,
  autoplayHoverPause: true,
  animateOut: "slideOutDown",
  animateIn: "flipInX",
  loop: true,
  margin: 10,
  nav: false,
  responsive: {
    0: {
      items: 1,
    },
    600: {
      items: 2,
    },
    1000: {
      items: 3,
    },
  },
});

document.addEventListener("DOMContentLoaded", function () {
  // Options for the IntersectionObserver
  var options = {
    root: null, // Use the viewport as the root
    threshold: 0.5, // Trigger when 50% of the element is visible
  };

  var observer = new IntersectionObserver(function (entries, observer) {
    entries.forEach(function (entry) {
      if (entry.isIntersecting) {
        animateCounter(entry.target); // Trigger animation when in view
        observer.unobserve(entry.target); // Stop observing once counted
      }
    });
  }, options);

  // Observe each .counter element
  document.querySelectorAll(".counter").forEach(function (counter) {
    observer.observe(counter);
  });

  function animateCounter(counter) {
    var $this = $(counter),
      countTo = $this.attr("data-count");

    $({ countNum: $this.text() }).animate(
      {
        countNum: countTo,
      },
      {
        duration: 3000,
        easing: "linear",
        step: function () {
          $this.text(Math.floor(this.countNum));
        },
        complete: function () {
          $this.text(this.countNum);
        },
      }
    );

    // Ensure counter is visible (fade-in effect)
    $this.css("opacity", "1");
  }
});

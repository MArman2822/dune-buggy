// for header

const dropdowns = document.querySelectorAll(".dropdown");
dropdowns.forEach((dropdown) => {
  dropdown.addEventListener("mouseenter", () => {
    dropdown.classList.add("show");
  });
  dropdown.addEventListener("mouseleave", () => {
    dropdown.classList.remove("show");
  });
});

//for language select option
document.addEventListener("DOMContentLoaded", function () {
  var langArray = [];
  var vodiapicker = document.querySelectorAll(".vodiapicker option");

  vodiapicker.forEach(function (option) {
    var img = option.getAttribute("data-thumbnail");
    var text = option.innerText;
    var value = option.value;
    var item =
      "<li> <span>" +
      text +
      '</span><img src="' +
      img +
      '" value="' +
      value +
      '"/>';
    langArray.push(item);
  });

  document.getElementById("a").innerHTML = langArray.join("");

  var btnSelect = document.querySelector(".btn-select");
  btnSelect.innerHTML = langArray[0];
  btnSelect.setAttribute("value", "en");

  var listItems = document.querySelectorAll("#a li");
  listItems.forEach(function (item) {
    item.addEventListener("click", function () {
      var img = this.querySelector("img").getAttribute("src");
      var value = this.querySelector("img").getAttribute("value");
      var text = this.innerText;
      var selectedItem =
        "<li><span>" + text + '</span> <img src="' + img + '"/></li>';
      btnSelect.innerHTML = selectedItem;
      btnSelect.setAttribute("value", value);
      document.querySelector(".b").classList.toggle("open");
    });
  });

  btnSelect.addEventListener("click", function () {
    document.querySelector(".b").classList.toggle("open");
  });

  var sessionLang = localStorage.getItem("lang");
  if (sessionLang) {
    var langIndex = langArray.findIndex(function (item) {
      return item.includes('value="' + sessionLang + '"');
    });
    btnSelect.innerHTML = langArray[langIndex];
    btnSelect.setAttribute("value", sessionLang);
  } else {
    var langIndex = langArray.findIndex(function (item) {
      return item.includes('value="en"');
    });
    btnSelect.innerHTML = langArray[langIndex];
    btnSelect.setAttribute("value", "en");
  }
});

// for active nav
document.addEventListener("DOMContentLoaded", function () {
  const currentLocation = location.href;
  const menuItem = document.querySelectorAll(".navbar-nav .nav-link");
  const menuLength = menuItem.length;
  for (let i = 0; i < menuLength; i++) {
    if (menuItem[i].href.replace(/#$/, "") === currentLocation) {
      menuItem[i].classList.add("active");
    }
  }
});

// for animate css
AOS.init();

function changeNavbarOnScroll() {
  let navbar = document.querySelector(".mainHeader");
  if (document.documentElement.scrollTop > 50) {
    navbar.classList.add("bgblur");
  } else {
    navbar.classList.remove("bgblur");
  }
}

const goTopButton = document.createElement("button");
goTopButton.id = "goTopButton";
goTopButton.innerHTML = '<i class="fa fa-chevron-up" aria-hidden="true"></i>';
document.body.appendChild(goTopButton);

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function () {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    goTopButton.style.display = "block";
  } else {
    goTopButton.style.display = "none";
  }
  changeNavbarOnScroll();
};

// When the user clicks on the button, scroll to the top of the document
goTopButton.addEventListener("click", function () {
  document.body.scrollTop = 0; // For Safari
  document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
});

// Create WhatsApp icon element
const whatsappIcon = document.createElement("a");
whatsappIcon.id = "whatsappIcon";
whatsappIcon.href = "https://api.whatsapp.com/send?phone=123456789"; // Replace with your WhatsApp link
whatsappIcon.target = "_blank";
whatsappIcon.innerHTML = '<i class="fa fa-whatsapp" aria-hidden="true"></i>';
document.body.appendChild(whatsappIcon);

// Show the WhatsApp icon
whatsappIcon.style.display = "block";

// Optionally, change style or behavior based on user interaction or other conditions
whatsappIcon.addEventListener("mouseover", function () {
  whatsappIcon.style.backgroundColor = "blue";
});

whatsappIcon.addEventListener("mouseout", function () {
  whatsappIcon.style.backgroundColor = "green";
});
